package Conexao;

import Entidades.Aluno;
import Entidades.Turma;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class AlunoDAO {

    private ConexaoDados banco = new ConexaoDados();

    public void insereAluno(Aluno aluno) throws SQLException {
        int retorno = 0;
        String sSQL = "INSERT INTO `caderneta`.`aluno_tb` (`nome`, `telefone`, `"
                + "turma`) VALUES ('" + aluno.getNome() + "', '"
                + aluno.getTelefone() + "', '" + aluno.getTurma().getID() + "');";
        retorno = banco.changeBanco(sSQL);
    }

    public void deletaAluno(int id) throws SQLException {
        String sSQL = "DELETE FROM `caderneta`.`aluno_tb` WHERE `idaluno`='" + id + "';";
        banco.changeBanco(sSQL);
    }

    public int alteraAlunoDados(Aluno aluno) throws SQLException {
        int retorno = 0;
        String sSQL = "UPDATE `caderneta`.`aluno_tb` SET `nome`='" + aluno.getNome()
                + "', `telefone`='" + aluno.getTelefone() + "', `turma`='" + aluno.getTurma().getID()
                + "' WHERE `idaluno`='" + aluno.getID() + "';";
        retorno = banco.changeBanco(sSQL);
        System.out.println(sSQL);
        return retorno;
    }

    public int alteraAlunoNotas(Aluno aluno) throws SQLException {
        int retorno = 0;
        String sSQL = "UPDATE `caderneta`.`aluno_tb` SET `nota1`='" + aluno.getNota1()
                + "', `nota2`='" + aluno.getNota2() + "', `nota3`='" + aluno.getNota3()
                + "', `nota4`='" + aluno.getNota4() + "' WHERE `idaluno`='" + aluno.getID() + "';";
        retorno = banco.changeBanco(sSQL);
        System.out.println(sSQL);
        return retorno;
    }

    public ArrayList<Aluno> listaAluno(Turma turma) throws SQLException {
        ArrayList<Aluno> alunos = new ArrayList<Aluno>();
        String sql = "select * from aluno_tb where turma=" + turma.getID() + " order by nome;";
        System.out.println(sql);
        ResultSet rs = banco.lista(sql);

        while (rs.next()) {

            int alunoID = rs.getInt(1);
            String nome = rs.getString(2);
            String telefone = rs.getString(3);
            double nota1 = rs.getDouble(4);
            double nota2 = rs.getDouble(5);
            double nota3 = rs.getDouble(6);
            double nota4 = rs.getDouble(7);

            alunos.add(new Aluno(alunoID, nome, telefone, nota1, nota2, nota3, nota4, turma));
        }
        return alunos;
    }
    public void fechaBanco() throws SQLException{
        banco.closeConexao();
    }
}
