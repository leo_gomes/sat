package Conexao;

import Entidades.Turma;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class TurmaDAO {

    private ConexaoDados banco = new ConexaoDados();

    public int insereTurma(Turma turma) throws SQLException {
        int retorno = 0;
        String sSQL = "INSERT INTO `caderneta`.`turma_tb` (`serie`, `turno`, `"
                + "escola`, `professor`) VALUES ('" + turma.getSerie() + "', '"
                + turma.getTurno() + "', '" + turma.getEscola().getID() + "', '"
                + turma.getProfessor().getID() + "');";
        retorno = banco.changeBanco(sSQL);
        return retorno;
    }

    public int deletaTurma(int id) throws SQLException {
        String sSQL = "DELETE FROM `caderneta`.`turma_tb` WHERE `idturma`='"
                + id + "';";
        return banco.changeBanco(sSQL);
    }

    public int alteraTurma(Turma turma) throws SQLException {
        int retorno = 0;
        String sSQL = "UPDATE `caderneta`.`turma_tb` SET `serie`='" + turma.getSerie()
                + "', `turno`='" + turma.getTurno() + "' WHERE `idturma`='" + turma.getID() + "';";
        retorno = banco.changeBanco(sSQL);
        return retorno;
    }

    public ArrayList listaTurma(int userId) throws SQLException {

        ArrayList turmas = new ArrayList();

        String sql = "select * from caderneta.turma_tb  inner join escola_tb on "
                + "turma_tb.escola = escola_tb.idescola where turma_tb.professor = "
                + userId + " order by escola_tb.nome ;";
        ResultSet rs = banco.lista(sql);

        while (rs.next()) {

            int turmaID = rs.getInt(1);
            String serie = rs.getString(2);
            String turno = rs.getString(3);
            int escolaId = rs.getInt(4);
            int professorId = rs.getInt(5);
            //pula o 6 pois ele é redundante, é o mesmo que escolaId
            String nomeEscola = rs.getString(7);
            String enderecoEscola = rs.getString(8);
            String foneEscola = rs.getString(9);

            turmas.add(new Object[]{turmaID, serie, turno, escolaId, professorId,
                nomeEscola, enderecoEscola, foneEscola});
        }
        return turmas;
    }
    public void fechaBanco() throws SQLException{
        banco.closeConexao();
    }
}
