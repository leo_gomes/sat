package Conexao;

import Entidades.Escola;
import Entidades.Professor;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class EscolaDAO {

    private ConexaoDados banco = new ConexaoDados();

    public int insereEscola(Escola esc) throws SQLException {
        int retorno = 0;
        String sSQL = "INSERT INTO `caderneta`.`escola_tb` (`nome`, `endereco`, `"
                + "telefone`) VALUES ('" + esc.getNome() + "', '" + esc.getEndereco() + "', '" + esc.getTelefone() + "');";
        retorno = banco.changeBanco(sSQL);
        return retorno;
    }

    public int deletaEscola(int id) throws SQLException {
        String sSQL = "DELETE FROM `caderneta`.`escola_tb` WHERE `idescola`='"
                + id + "';";
        return banco.changeBanco(sSQL);
    }

    public int alteraEscola(Escola esc) throws SQLException {
        int retorno = 0;
        String sSQL = "UPDATE `caderneta`.`escola_tb` SET `nome`='"
                + esc.getNome() + "', `endereco`='" + esc.getEndereco()
                + "', `telefone`='" + esc.getTelefone() + "' WHERE `idescola`='"
                + esc.getID() + "';";
        retorno = banco.changeBanco(sSQL);
        return retorno;
    }

    public ArrayList<Escola> listaEscola() throws SQLException {
        ArrayList<Escola> escolas = new ArrayList<Escola>();

        String sql = "SELECT * FROM caderneta.escola_tb;";
        ResultSet rs = banco.lista(sql);

        while (rs.next()) {

            int ID = rs.getInt(1);
            String nome = rs.getString(2);
            String endereco = rs.getString(3);
            String telefone = rs.getString(4);

            Escola school = new Escola(ID, nome, endereco, telefone);
            escolas.add(school);
        }
        return escolas;
    }
    public void fechaBanco() throws SQLException{
        banco.closeConexao();
    }
}
