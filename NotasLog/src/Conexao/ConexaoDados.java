package Conexao;

import java.sql.SQLException;
import java.sql.ResultSet;
import java.sql.Statement;
import javax.swing.JOptionPane;

public class ConexaoDados {

    private Statement stmt;
    private ResultSet rs;

    public ConexaoDados() {
    }

    public Statement getStmt() {
        return stmt;
    }

public void closeConexao() throws SQLException {
        if (rs != null) {
            rs.close();
        }
        if (stmt != null) {
            stmt.close();
        }
        ConexaoSGBD.getConexao().close();
    }

    public int changeBanco(String query) throws SQLException {
        int rows = 0;
        System.out.println(query);
        stmt = (Statement) ConexaoSGBD.getConexao().createStatement();
        rows = stmt.executeUpdate(query);
        //Perguntar ao professor o que retorna a chamada executeUpdate();
        stmt.close();
        return rows;
    }

    public ResultSet busca(String query) throws SQLException {
        stmt = (Statement) ConexaoSGBD.getConexao().createStatement();

        rs = (ResultSet) stmt.executeQuery(query);

        return rs;
    }

    public ResultSet lista(String query) throws SQLException {
        ResultSet resultset = null;

        stmt = (Statement) ConexaoSGBD.getConexao().createStatement();

        resultset = (ResultSet) stmt.executeQuery(query);

        return resultset;
    }
}
