package Conexao;

import Entidades.Professor;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ProfessorDAO {

    private ConexaoDados banco = new ConexaoDados();

    public int insereProfessor(Professor prof) throws SQLException {
        int retorno = 0;
        String sSQL = "INSERT INTO `caderneta`.`professor_tb` (`nome`, `CPF`, `"
                + "disciplina`) VALUES ('" + prof.getNome() + "', '"
                + prof.getCPF() + "', '" + prof.getDisciplina() + "');";
        retorno = banco.changeBanco(sSQL);
        return retorno;
    }

    public void deletaProfessor(int id) throws SQLException {
        String sSQL = "DELETE FROM `caderneta`.`professor_tb` WHERE `idprofessor`"
                + "='" + id + "';";
        banco.changeBanco(sSQL);
    }

    public Professor buscaProfessor(String user) throws SQLException {
        String sSQL = "SELECT * FROM caderneta.professor_tb where"
                + " nome = '" + user + "';";
        ResultSet rs = banco.busca(sSQL);
        Professor teacher;
        if (rs.absolute(1)) {
            rs.first();
            teacher = new Professor(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4));
        } else {
            teacher = new Professor(-1, "root", null, null);
            teacher.setID(-1);
        }

        return teacher;
    }

    public ArrayList<Professor> listaProfessor() throws SQLException {
        ArrayList<Professor> professores = new ArrayList<Professor>();
        return professores;
    }
    public void fechaBanco() throws SQLException{
        banco.closeConexao();
    }
}
