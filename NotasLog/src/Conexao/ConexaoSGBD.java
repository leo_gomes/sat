package Conexao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import javax.swing.JOptionPane;

public class ConexaoSGBD {

    private static Connection conexao;

    private ConexaoSGBD() {

    }

    public static Connection getConexao() {
        if (conexao == null) {
            try {
            //Esta linha serve para indicar o nome do Driver
                //para SQLSERVER use com.microsoft.sqlserver.jdbc.SQLServerDriver
                Class.forName("com.mysql.jdbc.Driver");
            //O caminho do seu banco de dados
                //para SQLSERVER use jdbc:sqlserver://localhost:1433;databaseName=teste;
                String url = "jdbc:mysql://localhost:3306/caderneta";
                //o nome do usuario do banco
                String user = "root";
                //a senha do usuario do banco
                String pass;
                pass = "1658";
                ConexaoSGBD.conexao = (Connection) DriverManager.getConnection(url, user, pass);
            } catch (SQLException e) {
                JOptionPane.showMessageDialog(null, "Falha ao conectar com o banco" + "\n" + e.getMessage());
            } catch (ClassNotFoundException e) {
                JOptionPane.showMessageDialog(null, "Falha ao conectar com o banco. Veja o log");
            }
        }
        return conexao;
    }
}