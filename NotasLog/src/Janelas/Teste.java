package Janelas;

import java.util.ArrayList;
import javax.swing.ListSelectionModel;
import java.lang.Object;
import java.text.DecimalFormat;

public class Teste extends javax.swing.JFrame {

    public Teste() {
        initComponents();
        //modelarTabelas();
    }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        texto45 = new javax.swing.JTextField();
        texto13 = new javax.swing.JTextField();
        textovotos = new javax.swing.JTextField();
        poraecio = new javax.swing.JTextField();
        pordilma = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("@ Pequisador da CHUCA! Versão 2.0");

        jButton1.setText("Ação");
        jButton1.setEnabled(false);
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jButton2.setText("Dilma 13");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        jButton3.setText("Aécio 45");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        texto45.setEditable(false);
        texto45.setText("0");

        texto13.setEditable(false);
        texto13.setText("0");
        texto13.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                texto13ActionPerformed(evt);
            }
        });

        textovotos.setEditable(false);
        textovotos.setText("0");

        poraecio.setEditable(false);
        poraecio.setText("0.0%");

        pordilma.setEditable(false);
        pordilma.setText("0.0%");

        jLabel1.setText("Votos:");

        jLabel2.setText("Votos:");

        jLabel3.setText("Total de votos:");

        jLabel4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icones/photo.png"))); // NOI18N

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(jLabel1)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(texto45)
                    .addComponent(poraecio, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jButton3, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 33, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jButton2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(texto13)))
                    .addComponent(pordilma, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 73, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(74, 74, 74))
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jButton1))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(112, 112, 112)
                        .addComponent(jLabel3)
                        .addGap(18, 18, 18)
                        .addComponent(textovotos, javax.swing.GroupLayout.PREFERRED_SIZE, 66, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(162, 162, 162)
                        .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(15, 15, 15)
                .addComponent(jLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, 55, Short.MAX_VALUE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(textovotos, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3))
                .addGap(27, 27, 27)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(poraecio, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(pordilma, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(texto45, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(texto13, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1)
                    .addComponent(jLabel2))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton2)
                    .addComponent(jButton3))
                .addGap(32, 32, 32)
                .addComponent(jButton1)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
//        ArrayList dados = new ArrayList();
//        dados.add(new Object[]{1, "viagem"});
//        dados.add(new Object[]{2, "luz do sol"});
//        Object[] lua = (Object[]) dados.get(0);
//        Object[] sol = (Object[]) dados.get(1);
//        System.out.println(lua[1]+" "+ lua [0] + " " +sol [1] + " " + sol [0]);
        // TODO add your handling code here:
    }//GEN-LAST:event_jButton1ActionPerformed

    private void texto13ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_texto13ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_texto13ActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        // TODO add your handling code here:
        aecio ++;
        texto45.setText(String.format("%.0f", aecio));
        votos();
        textovotos.setText(String.format("%.0f", voto));
        porcento();
    }//GEN-LAST:event_jButton3ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        // TODO add your handling code here:
        dilma++;
        texto13.setText(String.format("%.0f", dilma));
        votos();
        textovotos.setText(String.format("%.0f", voto));
        porcento();
    }//GEN-LAST:event_jButton2ActionPerformed
    public void votos (){
        this.voto = dilma + aecio;
    }
    public void porcento(){
        double x = (aecio * 100)/voto;
        double y = (dilma* 100)/voto;
        DecimalFormat fmt = new DecimalFormat("0.00");
        String novaa = fmt.format(y);
        String novad = fmt.format(x);
        pordilma.setText(novaa + "%");
        poraecio.setText(novad + "%");
    }
    public void modelarTabelas(){
//        ArrayList dados2 = new ArrayList();
//        String colunas2[] = new String [] {"  ID","Série", "Escola"};
//        dados2.add(new Object[]{"  ->", "Clique no Botão", "   <-"});
//        dados2.add(new Object[]{"  ->", "para preencher Tabela", "   <-"});
//        
//        TableModel modelo2 = new TableModel(dados2, colunas2);
//        Janelinha.setModel(modelo2);
//        Janelinha.getColumnModel().getColumn(0).setPreferredWidth(55);
//        Janelinha.getColumnModel().getColumn(0).setResizable(false);
//        Janelinha.getColumnModel().getColumn(1).setPreferredWidth(105);
//        Janelinha.getColumnModel().getColumn(1).setResizable(false);
//        Janelinha.getColumnModel().getColumn(2).setPreferredWidth(105);
//        Janelinha.getColumnModel().getColumn(2).setResizable(false);
//        Janelinha.getTableHeader().setReorderingAllowed(false);
//        Janelinha.setAutoResizeMode(Janelinha.AUTO_RESIZE_OFF);
//        Janelinha.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    }
//    public static void main(String args[]) {
//        /* Set the Nimbus look and feel */
//        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
//        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
//         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
//         */
//        try {
//            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
//                if ("Nimbus".equals(info.getName())) {
//                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
//                    break;
//                }
//            }
//        } catch (ClassNotFoundException ex) {
//            java.util.logging.Logger.getLogger(Teste.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (InstantiationException ex) {
//            java.util.logging.Logger.getLogger(Teste.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (IllegalAccessException ex) {
//            java.util.logging.Logger.getLogger(Teste.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
//            java.util.logging.Logger.getLogger(Teste.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        }
//        //</editor-fold>
//
//        /* Create and display the form */
//        java.awt.EventQueue.invokeLater(new Runnable() {
//            public void run() {
//                new Teste().setVisible(true);
//            }
//        });
//    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JTextField poraecio;
    private javax.swing.JTextField pordilma;
    private javax.swing.JTextField texto13;
    private javax.swing.JTextField texto45;
    private javax.swing.JTextField textovotos;
    // End of variables declaration//GEN-END:variables
    private double aecio = 0;
    private double dilma = 0;
    private double voto;
}
