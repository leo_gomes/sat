package Entidades;

public class Aluno {

    private int ID;
    private String nome;
    private String telefone;
    private double nota1;
    private double nota2;
    private double nota3;
    private double nota4;
    private double media;
    private Turma turma;

    public Aluno(int ID, String nome, String telefone, int idTurma) {
        if (turma == null) {
            turma = new Turma(idTurma, null, null);
        } else {
            this.turma.setID(idTurma);
        }
        this.ID = ID;
        this.nome = nome;
        this.telefone = telefone;
    }

    public Aluno(int ID, String nome, String telefone, double nota1, double nota2, double nota3, double nota4, Turma turma) {
        this.ID = ID;
        this.nome = nome;
        this.telefone = telefone;
        this.nota1 = nota1;
        this.nota2 = nota2;
        this.nota3 = nota3;
        this.nota4 = nota4;
        this.turma = turma;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public double getNota1() {
        return nota1;
    }

    public void setNota1(double nota1) {
        this.nota1 = nota1;
    }

    public double getNota2() {
        return nota2;
    }

    public void setNota2(double nota2) {
        this.nota2 = nota2;
    }

    public double getNota3() {
        return nota3;
    }

    public void setNota3(double nota3) {
        this.nota3 = nota3;
    }

    public double getNota4() {
        return nota4;
    }

    public void setNota4(double nota4) {
        this.nota4 = nota4;
    }

    public double getMedia() {
        return media;
    }

    public void setMedia(double media) {
        this.media = media;
    }

    public Turma getTurma() {
        return turma;
    }

    public void setTurma(Turma turma) {
        this.turma = turma;
    }

    public void calcMedia() {
        int contNotas = 0;
        if ((nota1 > 0) && (nota1 <= 10)) {
            contNotas++;
        }
        if ((nota2 > 0) && (nota2 <= 10)) {
            contNotas++;
        }
        if ((nota3 > 0) && (nota3 <= 10)) {
            contNotas++;
        }
        if ((nota4 > 0) && (nota4 <= 10)) {
            contNotas++;
        }
        if (contNotas != 0) {
            this.media = (nota1 + nota2 + nota3 + nota4) / contNotas;
        } else {
            this.media = 0;
        }
    }

    @Override
    public String toString() {
        return "Aluno{" + "ID=" + ID + ", nome=" + nome + "\ntelefone=" + telefone + ", turma=" + turma + '}';
    }
}
