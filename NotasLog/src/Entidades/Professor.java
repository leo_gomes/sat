package Entidades;

public class Professor {

    private int ID;
    private String nome;
    private String CPF;
    private String disciplina;
    //private ArrayList<Escola> escolas;

    public Professor(int ID, String nome, String CPF, String disciplina) {
        this.ID = ID;
        this.nome = nome;
        this.CPF = CPF;
        this.disciplina = disciplina;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCPF() {
        return CPF;
    }

    public void setCPF(String CPF) {
        this.CPF = CPF;
    }

    public String getDisciplina() {
        return disciplina;
    }

    public void setDisciplina(String disciplina) {
        this.disciplina = disciplina;
    }
}
