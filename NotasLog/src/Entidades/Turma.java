package Entidades;

import java.util.ArrayList;

public class Turma {

    private int ID;
    private String serie;
    private String turno;
    private Escola escola;
    private Professor professor;

    public Turma(int ID, String serie, String turno, int idEscola, int idProfessor) {
        if (escola == null) {
            escola = new Escola(idEscola, null, null, null);
        }
        if (professor == null) {
            professor = new Professor(idProfessor, null, null, null);
        }
        this.ID = ID;
        this.serie = serie;
        this.turno = turno;
        this.escola.setID(idEscola);
        this.professor.setID(idProfessor);
    }

    public Turma(int ID, String serie, String turno) {
        this.ID = ID;
        this.serie = serie;
        this.turno = turno;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getSerie() {
        return serie;
    }

    public void setSerie(String serie) {
        this.serie = serie;
    }

    public String getTurno() {
        return turno;
    }

    public void setTurno(String turno) {
        this.turno = turno;
    }

    public Escola getEscola() {
        return escola;
    }

    public void setEscola(Escola escola) {
        this.escola = escola;
    }

    public Professor getProfessor() {
        return professor;
    }

    public void setProfessor(Professor professor) {
        this.professor = professor;
    }
}
