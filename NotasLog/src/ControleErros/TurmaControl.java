package ControleErros;

import Conexao.TurmaDAO;
import Entidades.Turma;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.swing.JOptionPane;

public class TurmaControl {

    private TurmaDAO bancoTurma = new TurmaDAO();

    public boolean cadastroControl(Turma turma) throws SQLException {
        boolean ret = false;
        if ((turma.getSerie() != null) && (turma.getTurno() != null)) {
            if ((turma.getSerie().equals("")) || (turma.getTurno().equals(""))) {
                JOptionPane.showMessageDialog(null, "Há algum campo vazio.");
            } else if ((turma.getEscola().getID() == 0) || (turma.getProfessor().getID() == 0)) {
                JOptionPane.showMessageDialog(null, "Escolha ao menos uma opção"
                        + " da lista de escola onde essa turma será inserida..");
            } else {
                bancoTurma.insereTurma(turma);
                ret = true;
            }
        } else {
            JOptionPane.showMessageDialog(null, "Há Campos vazios.");
        }
        return ret;
    }

    public boolean alteraControl(Turma turma, Turma antiga) throws SQLException {
        boolean ret = false;
        if ((turma.getSerie() != null) && (turma.getTurno() != null)) {
            if ((turma.getSerie().equals("")) || (turma.getTurno().equals(""))) {
                JOptionPane.showMessageDialog(null, "Há Campos vazios.");
            } else if ((antiga.getSerie().equals(turma.getSerie()))
                    && (antiga.getTurno().equals(turma.getTurno()))) {
                JOptionPane.showMessageDialog(null, "Nenhuma mudança foi detectada.");
            } else {
                bancoTurma.alteraTurma(turma);
                ret = true;
            }
        } else {
            JOptionPane.showMessageDialog(null, "Há Campos vazios.");
        }
        return ret;
    }

    public void deletaControl(int id) throws SQLException {
        bancoTurma.deletaTurma(id);

    }

    public ArrayList retornaTurmasConcatena(int userId) throws SQLException {
        //Lista as turmas com a série e turno concatenados em uma tabela onde 
        //não será permitido ao usuario apagada alguma turma.
        ArrayList retorno = new ArrayList();
        ArrayList dados = bancoTurma.listaTurma(userId);
        //Faz o método retornar um array vazio se não tiver dados no Banco de Dados
        if (dados.isEmpty() == false) {
            for (int i = 0; i < dados.size(); i++) {
                Object[] aux = (Object[]) dados.get(i);
                //String serieTurno = aux[1]+" "+aux[2];// uni série e turno
                retorno.add(new Object[]{aux[0], aux[1] + " " + aux[2], aux[5]});
            }
        }

        return retorno;
    }

    public ArrayList retornaTurmas(int userId) throws SQLException {
        //Lista as turmas com ID, série, turno e o nome da escola pertencente.
        ArrayList retorno = new ArrayList();
        ArrayList dados = bancoTurma.listaTurma(userId);
        //Faz o método retornar um array vazio se não tiver dados no Banco de Dados
        if (dados.isEmpty() == false) {
            for (int i = 0; i < dados.size(); i++) {
                Object[] aux = (Object[]) dados.get(i);
                //ID turma, série, turno, id escola, nome escola.
                retorno.add(new Object[]{aux[0], aux[1], aux[2], aux[3], aux[5]});
            }
        }
        return retorno;
    }
    public void fecha() throws SQLException{
        bancoTurma.fechaBanco();
    }
}
