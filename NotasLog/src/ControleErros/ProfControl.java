package ControleErros;

import Conexao.ProfessorDAO;
import Entidades.Professor;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.swing.JOptionPane;

public class ProfControl {

    private ProfessorDAO bancoProf = new ProfessorDAO();

    public boolean cadastroControl(Professor prof) throws SQLException {
        boolean ret = false;
        if ((prof.getNome() != null) && (prof.getCPF() != null) && (prof.getDisciplina() != null)) {
            if ((prof.getNome().equals("")) || (prof.getCPF().equals("")) || (prof.getDisciplina().equals(""))) {
                JOptionPane.showMessageDialog(null, "Há Campos vazios.");
            } else {
                bancoProf.insereProfessor(prof);
                ret = true;
            }
        } else {
            JOptionPane.showMessageDialog(null, "Há Campos vazios.");
        }
        return ret;
    }

    public Professor buscaControl(String user, Professor usuario) throws SQLException {
        if ((user == null) || (user.equals("")) || (user.equals("Digite seu nome aqui."))) {

        } else {
            usuario = bancoProf.buscaProfessor(user);
        }
        return usuario;
    }

    public void deletaProf(int id) throws SQLException {
        bancoProf.deletaProfessor(id);
    }
    public void fecha() throws SQLException{
        bancoProf.fechaBanco();
    }
}
