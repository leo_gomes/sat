package ControleErros;

import Conexao.AlunoDAO;
import Entidades.Aluno;
import Entidades.Turma;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import javax.swing.JOptionPane;

public class AlunoControl {

    private AlunoDAO bancoAluno = new AlunoDAO();

    public boolean cadastroAluno(Aluno aluno) throws SQLException {
        boolean ret = false;
        if ((aluno.getNome() != null) && (aluno.getTurma().getID() != 0)) {
            if ((aluno.getNome().equals("")) || (aluno.getTurma().getID() == 0)) {
                JOptionPane.showMessageDialog(null, "Há Campos vazios.");
            } else {
                bancoAluno.insereAluno(aluno);
                ret = true;
            }
        } else {
            JOptionPane.showMessageDialog(null, "Há Campos vazios.");
        }
        return ret;
    }

    public void deletaAluno(int id) throws SQLException {
        bancoAluno.deletaAluno(id);
    }

    public boolean alteraDados(Aluno aluno, Aluno antigo) throws SQLException {
        boolean ret = false;
        if ((aluno.getNome() != null) && (aluno.getTelefone() != null)) {
            if (aluno.getNome().equals("")) {
                JOptionPane.showMessageDialog(null, "Há campos vazios");
            } else if (aluno.getTurma().getID() <= 0) {
                JOptionPane.showMessageDialog(null, "Números negativos não são permitidos no Banco de dados!");
            } else if ((aluno.getNome().equals(antigo.getNome())
                    && (aluno.getTelefone().equals(antigo.getTelefone()))
                    && (aluno.getTurma().getID() == antigo.getTurma().getID()))) {
                JOptionPane.showMessageDialog(null, "Nenhuma mudança foi detectada.");
            } else {
                bancoAluno.alteraAlunoDados(aluno);
                ret = true;
            }
        } else {
            JOptionPane.showMessageDialog(null, "Há Campos vazios.");
        }
        return ret;
    }

    public boolean alteraNotas(Aluno aluno) throws SQLException {
        boolean ret = false;
        if ((aluno.getNota1() < 0) || (aluno.getNota2() < 0) || (aluno.getNota3() < 0) || (aluno.getNota4() < 0)) {
            JOptionPane.showMessageDialog(null, "Números negativos não são permitidos no Banco de dados!");
        } else if ((aluno.getNota1() > 10) || (aluno.getNota2() > 10) || (aluno.getNota3() > 10) || (aluno.getNota4() > 10)) {
            JOptionPane.showMessageDialog(null, "Apenas números reais entre 0 e 10 são permitidos "
                    + "nas notas dos alunos!");
        } else {
            bancoAluno.alteraAlunoNotas(aluno);
            ret = true;
        }
        System.out.println("Compila mudiça");
        return ret;
    }

    public ArrayList retornaAlunos(Turma turma) throws SQLException {
        ArrayList retorno = new ArrayList();
        ArrayList<Aluno> dados = bancoAluno.listaAluno(turma);
        if (dados.isEmpty() == false) {
            for (int i = 0; i < dados.size(); i++) {
                Aluno aluno = dados.get(i);
                aluno.calcMedia();
                DecimalFormat fmt = new DecimalFormat("0.00");
                //média com 2 casas decimais.
                String nova = fmt.format(aluno.getMedia());
                retorno.add(new Object[]{aluno.getID(), aluno.getNome(), aluno.getNota1(),
                    aluno.getNota2(), aluno.getNota3(), aluno.getNota4(), nova, aluno.getTelefone()});
            }
        } else {
            JOptionPane.showMessageDialog(null, "Não há alunos cadastrados nesta turma."
                    + "\nVá ao menu Arquivo > Cadastrar Coponentes na aba alunos para "
                    + "\ncadastrar novos alunos.");
            retorno.add(new Object[]{"-1", "Vazio", "Vazio", "0", "0", "0", "0", "0", "Vazio"});
        }
        return retorno;
    }
    public void fecha() throws SQLException{
        bancoAluno.fechaBanco();
    }
}
