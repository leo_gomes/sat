package ControleErros;

import Conexao.EscolaDAO;
import Entidades.Escola;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;

public class EscControl {

    private EscolaDAO bancoEscola = new EscolaDAO();

    public boolean cadastroControl(Escola escola) throws SQLException {
        boolean ret = false;
        if ((escola.getNome() != null) && (escola.getEndereco() != null)) {
            if ((escola.getNome().equals("")) || (escola.getEndereco().equals(""))) {
                JOptionPane.showMessageDialog(null, "Há Campos vazios.");
            } else {
                bancoEscola.insereEscola(escola);
                ret = true;
            }
        } else {
            JOptionPane.showMessageDialog(null, "Há Campos vazios.");
        }
        return ret;
    }

    public boolean alteraControl(Escola escola, Escola antiga) throws SQLException {
        boolean ret = false;
        if ((escola.getNome() != null) && (escola.getEndereco() != null)) {
            if ((escola.getNome().equals("")) || (escola.getEndereco().equals(""))) {
                JOptionPane.showMessageDialog(null, "Há Campos vazios.");
            } else if ((antiga.getNome().equals(escola.getNome()))
                    && (antiga.getEndereco().equals(escola.getEndereco()))
                    && (antiga.getTelefone().equals(escola.getTelefone()))) {
                JOptionPane.showMessageDialog(null, "Nenhuma mudança foi detectada.");
            } else {
                bancoEscola.alteraEscola(escola);
                ret = true;
            }
        } else {
            JOptionPane.showMessageDialog(null, "Há Campos vazios.");
        }
        return ret;
    }

    public void deletaControl(int id) throws SQLException {
        bancoEscola.deletaEscola(id);

    }

    public ArrayList<Escola> retornaEscolas() throws SQLException {

        //realiza algum processamento se necessario
        return bancoEscola.listaEscola();

    }
    public void fecha() throws SQLException{
        bancoEscola.fechaBanco();
    }
}
